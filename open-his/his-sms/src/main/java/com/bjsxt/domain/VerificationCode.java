package com.bjsxt.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@ApiModel(value="com-bjsxt-domain-verificationCode")
@Data
@EqualsAndHashCode(callSuper=true)
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "his_verification_code")
public class VerificationCode extends BaseEntity{

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    @ApiModelProperty(value = "ID")
    private String id;

    @TableField(value = "verification_code")
    @ApiModelProperty(value = "验证码")
    private Integer verificationCode;

    @TableField(value = "phone_number")
    @ApiModelProperty(value = "手机号")
    private String phoneNumber;

    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private String createTime;

    @TableField(value = "is_check")
    @ApiModelProperty(value = "是否校验")
    private Integer isCheck;

    public static final String VERIFICATION_CODE = "verification_code";

    public static final String PHONE_NUMBER = "phone_number";

    public static final String IS_CHECK = "is_check";
}
