package com.bjsxt.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bjsxt.config.SmsSendConfig;
import com.bjsxt.domain.VerificationCode;
import com.bjsxt.mapper.VerificationCodeMapper;
import com.bjsxt.service.SmsSendService;
import com.bjsxt.utils.HttpUtils;
import com.bjsxt.utils.IdGeneratorSnowflake;
import org.apache.commons.lang.StringUtils;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.URLEncoder;

@Service
public class SmsSendServiceImpl implements SmsSendService {

    @Autowired
    private VerificationCodeMapper verificationCodeMapper;

    @Autowired
    private SmsSendConfig smsSendConfig;

    /**
     * 短信发送(验证码通知，会员营销)
     * 接口文档地址：http://www.danmi.com/developer.html#smsSend
     */
    @Override
    public String execute(String phoneNumber, Integer code) throws Exception{
        StringBuilder sb = new StringBuilder();
        sb.append("accountSid").append("=").append(smsSendConfig.getACCOUNT_SID());  //客户id
        sb.append("&to").append("=").append(phoneNumber);
        sb.append("&templateid").append("=").append(smsSendConfig.getTEMPLATE_ID());     //模板id
        sb.append("&param").append("=").append(URLEncoder.encode(code.toString(),"UTF-8"));
//		sb.append("&smsContent").append("=").append( URLEncoder.encode("【旦米科技】您的验证码为123456，该验证码5分钟内有效。请勿泄漏于他人。","UTF-8"));
        String body = sb.toString() + smsSendConfig.createCommonParam(smsSendConfig.getACCOUNT_SID(), smsSendConfig.getAUTH_TOKEN());
        return HttpUtils.post(smsSendConfig.getBASE_URL(), body);
    }

    /**
     * 发送短信
     * @param phoneNumber
     */
    @Override
    public int sendSms(String phoneNumber){
        Integer code = IdGeneratorSnowflake.generateVerificationCode();
        try {
            String result = this.execute(phoneNumber,code);
            JSONObject jsonObject = (JSONObject) JSON.parse(result);
            if(jsonObject != null && StringUtils.isNotEmpty(jsonObject.getString("respCode"))){
                if(jsonObject.getString("respCode").equals("0000")){
                    return saveVerificationCode(phoneNumber,code);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * 发送成功时将验证码存入his_verification_code表
     * @param phoneNumber
     * @param code
     * @return
     */
    @Override
    public int saveVerificationCode(String phoneNumber, Integer code){
        VerificationCode verificationCode=new VerificationCode();
        verificationCode.setPhoneNumber(phoneNumber);
        verificationCode.setVerificationCode(code);
        verificationCode.setCreateTime(DateUtil.date().toString());
        verificationCode.setIsCheck(0);
        return verificationCodeMapper.insert(verificationCode);
    }

    /**
     *
     * @param phoneNumber
     * @param code
     * @return
     */
    @Override
    public VerificationCode checkCode(String phoneNumber, Integer code) {
        QueryWrapper<VerificationCode> qw=new QueryWrapper<>();
        qw.eq(VerificationCode.PHONE_NUMBER,phoneNumber);
        qw.eq(VerificationCode.VERIFICATION_CODE,code);
        qw.eq(VerificationCode.IS_CHECK,0);
        return verificationCodeMapper.selectOne(qw);
    }
}
