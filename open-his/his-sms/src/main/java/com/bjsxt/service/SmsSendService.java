package com.bjsxt.service;

import com.bjsxt.domain.VerificationCode;

public interface SmsSendService {

    String execute(String phoneNumber, Integer code) throws Exception;

    int sendSms(String phoneNumber);

    int saveVerificationCode(String phoneNumber, Integer code);

    VerificationCode checkCode(String phoneNumber, Integer code);
}
