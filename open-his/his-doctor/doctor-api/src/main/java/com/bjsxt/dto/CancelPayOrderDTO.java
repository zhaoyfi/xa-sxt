package com.bjsxt.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CancelPayOrderDTO implements Serializable{
	
	/**
	 */
	private static final long serialVersionUID = -191417001399470940L;
	
	private String orderId;
}
