package com.bjsxt.listener;

import com.alibaba.fastjson.JSON;
import com.bjsxt.constants.MQConstant;
import com.bjsxt.dto.CancelPayOrderDTO;
import com.bjsxt.mq.dto.BaseMqDTO;
import com.bjsxt.mq.listener.AbstractMqListener;
import com.bjsxt.service.OrderChargeService;
import com.bjsxt.utils.ValidateUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RocketMQMessageListener(topic = MQConstant.Topic.CANCEL_PAY_ORDER, consumerGroup = "${rocketmq.producer.group}" + "_" + MQConstant.Topic.CANCEL_PAY_ORDER)
public class CancelPayOrderListener extends AbstractMqListener<BaseMqDTO<CancelPayOrderDTO>> implements RocketMQListener<BaseMqDTO<CancelPayOrderDTO>>{
	
	@Autowired
	private OrderChargeService orderChargeService;

    @Override
    public void onMessage(BaseMqDTO<CancelPayOrderDTO> data) {
    	log.info("onMessage - data:{}",JSON.toJSONString(data));
    	ValidateUtils.notNullParam(data);
    	ValidateUtils.notNullParam(data.getMessageId());
    	
		CancelPayOrderDTO cancelPayOrderDTO = data.getData();
    	ValidateUtils.notNullParam(cancelPayOrderDTO);
    	
    	String orderId = cancelPayOrderDTO.getOrderId();
    	ValidateUtils.notNullParam(orderId);
		//更新订单状态为已取消
		orderChargeService.cancelPayOrder(cancelPayOrderDTO);
    }
}