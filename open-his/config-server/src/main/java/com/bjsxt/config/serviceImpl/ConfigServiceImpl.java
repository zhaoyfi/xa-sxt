package com.bjsxt.config.serviceImpl;

import com.bjsxt.config.service.ConfigService;
import org.apache.dubbo.config.annotation.Service;

@Service
public class ConfigServiceImpl implements ConfigService {


    @Override
    public void configRegistry() {
        System.out.println("config server is registered on dubbo.....");
    }
}
