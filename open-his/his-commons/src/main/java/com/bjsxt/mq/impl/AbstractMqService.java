package com.bjsxt.mq.impl;

import com.alibaba.fastjson.JSON;
import com.bjsxt.mq.dto.BaseMqDTO;
import com.bjsxt.mq.service.RocketMqService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;

@Slf4j
public abstract class AbstractMqService implements RocketMqService {

    @Retryable(value = {Exception.class}, maxAttempts = 3, backoff = @Backoff(delay = 5000L, multiplier = 2))
    abstract public void convertAndSend(String topic,BaseMqDTO<?> data);

    @Retryable(value = {Exception.class}, maxAttempts = 3, backoff = @Backoff(delay = 5000L, multiplier = 2))
    abstract public void sendDelayed(String topic, BaseMqDTO<?> data, int delayLevel);

    @Recover
    public void recover(Exception ex, Object arg0,Object arg1) {
    	//TODO 后续可以考虑持久化&报警
        log.error("AbstractMqService - recover - args0:{} arg1:{} ex",JSON.toJSONString(arg0),JSON.toJSONString(arg1),ex);
    }


}
