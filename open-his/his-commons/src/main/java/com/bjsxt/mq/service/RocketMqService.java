package com.bjsxt.mq.service;

import com.bjsxt.mq.dto.BaseMqDTO;

public interface RocketMqService {

	void convertAndSend(String topic, BaseMqDTO<?> data);

	void sendDelayed(String topic,BaseMqDTO<?> data,int delayLevel);
}
