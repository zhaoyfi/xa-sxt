package com.bjsxt.mq.listener;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;

@Slf4j
public abstract class AbstractMqListener<T>{

	@Retryable(value = {Exception.class}, maxAttempts = 3, backoff = @Backoff(delay = 5000L, multiplier = 2))
    abstract public void onMessage(T message);

    @Recover
    public void recover(Exception ex, Object arg0) {
    	//TODO 后续可以考虑持久化&报警
    	log.error("AbstractMqListener - recover - arg0:{} ex",JSON.toJSONString(arg0),ex);
    }
}
