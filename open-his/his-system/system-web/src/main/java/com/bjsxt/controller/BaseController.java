package com.bjsxt.controller;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.bjsxt.vo.AjaxResult;

/**
 * @Author: 尚学堂 雷哥
 */
public class BaseController {

    /**
     * 如远程服务不可用，或者出现异常，回调的方法
     * @return
     */
//    public AjaxResult fallback(){
//        return AjaxResult.fail("服务器内部异常，请联系管理员");
//    }

    // 注意：方法是静态的
    public static AjaxResult handleException(String phoneNumber,Integer code, BlockException blockException) {
        return AjaxResult.fail("您访问的太频繁了，请稍后再点击");
    }

    public static AjaxResult handleError(String phoneNumber,Integer code) {
        return AjaxResult.fail("服务降级....");
    }
}
