package com.bjsxt.controller.sms;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.bjsxt.controller.BaseController;
import com.bjsxt.service.SmsSendService;
import com.bjsxt.vo.AjaxResult;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("system/sms")
public class SmsSendController{

    @Reference
    private SmsSendService smsSendService;

    /**
     * 获取验证码
     * @param phoneNumber
     * @return
     */
    @PostMapping("/sendSms/{phoneNumber}")
    @SentinelResource(blockHandlerClass = BaseController.class, blockHandler = "handleException",
            fallbackClass = BaseController.class,fallback = "handleError")
    public AjaxResult acquireVerifyCode(@PathVariable String phoneNumber){
        return AjaxResult.toAjax(smsSendService.sendSms(phoneNumber));
    }

    /**
     * 校验验证码
     * @param phoneNumber
     * @param code
     * @return
     */
    @GetMapping("/checkCode/{phoneNumber}/{code}")
    @SentinelResource(value = "checkVerifyCode", blockHandlerClass = BaseController.class, blockHandler = "handleException",
            fallbackClass = BaseController.class,fallback = "handleError")
    public AjaxResult checkVerifyCode(@PathVariable String phoneNumber, @PathVariable Integer code){
//        int a = 1/0;
        return AjaxResult.success(smsSendService.checkCode(phoneNumber,code));
    }

}
