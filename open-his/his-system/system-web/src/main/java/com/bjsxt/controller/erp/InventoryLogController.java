package com.bjsxt.controller.erp;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.bjsxt.controller.BaseController;
import com.bjsxt.dto.InventoryLogDto;
import com.bjsxt.service.InventoryLogService;
import com.bjsxt.vo.AjaxResult;
import com.bjsxt.vo.DataGridView;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: 尚学堂 雷哥
 * @Description:入库记录控制器
 */
@RestController
@RequestMapping("erp/inventoryLog")
public class InventoryLogController {


    @Reference//使用dubbo的引用
    private InventoryLogService inventoryLogService;

    /**
     * 分页查询
     */
    @GetMapping("listInventoryLogForPage")
    @SentinelResource(blockHandlerClass = BaseController.class, blockHandler = "handleException",
            fallbackClass = BaseController.class,fallback = "handleError")
    public AjaxResult listMedicinesForPage(InventoryLogDto inventoryLogDto){
        DataGridView gridView = this.inventoryLogService.listInventoryLogPage(inventoryLogDto);
        return AjaxResult.success("查询成功",gridView.getData(),gridView.getTotal());
    }

}
