package com.bjsxt.controller.erp;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.bjsxt.aspectj.annotation.Log;
import com.bjsxt.aspectj.enums.BusinessType;
import com.bjsxt.controller.BaseController;
import com.bjsxt.dto.MedicinesDto;
import com.bjsxt.service.MedicinesService;
import com.bjsxt.utils.ShiroSecurityUtils;
import com.bjsxt.vo.AjaxResult;
import com.bjsxt.vo.DataGridView;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @Auther: 尚学堂 雷哥
 * @Description:药品信息控制器
 */
@RestController
@RequestMapping("erp/medicines")
public class MedicinesController {


    @Reference//使用dubbo的引用
    private MedicinesService medicinesService;

    /**
     * 分页查询
     */
    @GetMapping("listMedicinesForPage")
    @SentinelResource(value = "listMedicinesForPage", blockHandlerClass = BaseController.class,
            blockHandler = "handleException",fallbackClass = BaseController.class,fallback = "handleError")
    public AjaxResult listMedicinesForPage(MedicinesDto medicinesDto){
        DataGridView gridView = this.medicinesService.listMedicinesPage(medicinesDto);
        return AjaxResult.success("查询成功",gridView.getData(),gridView.getTotal());
    }
    /**
     * 添加
     */
    @PostMapping("addMedicines")
    @SentinelResource(value = "addMedicines", blockHandlerClass = BaseController.class,
            blockHandler = "handleException",fallbackClass = BaseController.class,fallback = "handleError")
    @Log(title = "添加药品信息",businessType = BusinessType.INSERT)
    public AjaxResult addMedicines(@Validated MedicinesDto medicinesDto) {
        medicinesDto.setSimpleUser(ShiroSecurityUtils.getCurrentSimpleUser());
        return AjaxResult.toAjax(this.medicinesService.addMedicines(medicinesDto));
    }

    /**
     * 修改
     */
    @PutMapping("updateMedicines")
    @SentinelResource(value = "updateMedicines", blockHandlerClass = BaseController.class,
            blockHandler = "handleException",fallbackClass = BaseController.class,fallback = "handleError")
    @Log(title = "修改药品信息",businessType = BusinessType.UPDATE)
    public AjaxResult updateMedicines(@Validated MedicinesDto medicinesDto) {
        medicinesDto.setSimpleUser(ShiroSecurityUtils.getCurrentSimpleUser());
        return AjaxResult.toAjax(this.medicinesService.updateMedicines(medicinesDto));
    }


    /**
     * 根据ID查询一个药品信息信息
     */
    @GetMapping("getMedicinesById/{medicinesId}")
    @SentinelResource(value = "getMedicinesById", blockHandlerClass = BaseController.class,
            blockHandler = "handleException",fallbackClass = BaseController.class,fallback = "handleError")
    public AjaxResult getMedicinesById(@PathVariable @Validated @NotNull(message = "药品信息ID不能为空") Long medicinesId) {
        return AjaxResult.success(this.medicinesService.getOne(medicinesId));
    }

    /**
     * 删除
     */
    @DeleteMapping("deleteMedicinesByIds/{medicinesIds}")
    @SentinelResource(value = "deleteMedicinesByIds", blockHandlerClass = BaseController.class,
            blockHandler = "handleException",fallbackClass = BaseController.class,fallback = "handleError")
    @Log(title = "删除药品信息",businessType = BusinessType.DELETE)
    public AjaxResult deleteMedicinesByIds(@PathVariable @Validated @NotEmpty(message = "要删除的ID不能为空") Long[] medicinesIds) {
        return AjaxResult.toAjax(this.medicinesService.deleteMedicinesByIds(medicinesIds));
    }

    /**
     * 查询所有可用的药品信息
     */
    @SentinelResource(value = "selectAllMedicines", blockHandlerClass = BaseController.class,
            blockHandler = "handleException",fallbackClass = BaseController.class,fallback = "handleError")
    @GetMapping("selectAllMedicines")
    public AjaxResult selectAllMedicines() {
        return AjaxResult.success(this.medicinesService.selectAllMedicines());
    }

    /**
     * 调整库存
     */
    @SentinelResource(value = "xx", blockHandlerClass = BaseController.class,
            blockHandler = "handleException",fallbackClass = BaseController.class,fallback = "handleError")
    @Log(title = "调整药品库存信息",businessType = BusinessType.UPDATE)
    @PostMapping("updateMedicinesStorage/{medicinesId}/{medicinesStockNum}")
    public AjaxResult xx(@PathVariable Long medicinesId,@PathVariable Long medicinesStockNum){
        int i = this.medicinesService.updateMedicinesStorage(medicinesId, medicinesStockNum);
        return AjaxResult.toAjax(i);
    }

}
